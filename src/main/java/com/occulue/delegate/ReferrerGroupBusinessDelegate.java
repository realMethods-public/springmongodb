/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.*;

/**
 * ReferrerGroup business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of ReferrerGroup related services in the case of a ReferrerGroup business related service failing.</li>
 * <li>Exposes a simpler, uniform ReferrerGroup interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill ReferrerGroup business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class ReferrerGroupBusinessDelegate 
extends BaseBusinessDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public ReferrerGroupBusinessDelegate() 
	{
	}

        
   /**
	* ReferrerGroup Business Delegate Factory Method
	*
	* Returns a singleton instance of ReferrerGroupBusinessDelegate().
	* All methods are expected to be self-sufficient.
	*
	* @return 	ReferrerGroupBusinessDelegate
	*/
	public static ReferrerGroupBusinessDelegate getReferrerGroupInstance()
	{
	    if ( singleton == null )
	    {
	    	singleton = new ReferrerGroupBusinessDelegate();
	    }
	    
	    return( singleton );
	}
 
    /**
     * Method to retrieve the ReferrerGroup via an ReferrerGroupPrimaryKey.
     * @param 	key
     * @return 	ReferrerGroup
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException 
     */
    public ReferrerGroup getReferrerGroup( ReferrerGroupPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "ReferrerGroupBusinessDelegate:getReferrerGroup - ";
        if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        ReferrerGroup returnBO = null;
                
        ReferrerGroupDAO dao = getReferrerGroupDAO();
        
        try
        {
            returnBO = dao.findReferrerGroup( key );
        }
        catch( Exception exc )
        {
            String errMsg = "ReferrerGroupBusinessDelegate:getReferrerGroup( ReferrerGroupPrimaryKey key ) - unable to locate ReferrerGroup with key " + key.toString() + " - " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferrerGroupDAO( dao );
        }        
        
        return returnBO;
    }


    /**
     * Method to retrieve a collection of all ReferrerGroups
     *
     * @return 	ArrayList<ReferrerGroup> 
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<ReferrerGroup> getAllReferrerGroup() 
    throws ProcessingException
    {
    	String msgPrefix				= "ReferrerGroupBusinessDelegate:getAllReferrerGroup() - ";
        ArrayList<ReferrerGroup> array	= null;
        
        ReferrerGroupDAO dao = getReferrerGroupDAO();
    
        try
        {
            array = dao.findAllReferrerGroup();
        }
        catch( Exception exc )
        {
            String errMsg = msgPrefix + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferrerGroupDAO( dao );
        }        
        
        return array;
    }

   /**
    * Creates the provided BO.
    * @param		businessObject 	ReferrerGroup
    * @return       ReferrerGroup
    * @exception    ProcessingException
    * @exception	IllegalArgumentException
    */
	public ReferrerGroup createReferrerGroup( ReferrerGroup businessObject )
    throws ProcessingException, IllegalArgumentException
    {
		String msgPrefix = "ReferrerGroupBusinessDelegate:createReferrerGroup - ";
		
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        // return value once persisted
        ReferrerGroupDAO dao  = getReferrerGroupDAO();
        
        try
        {
            businessObject = dao.createReferrerGroup( businessObject );
        }
        catch (Exception exc)
        {
            String errMsg = "ReferrerGroupBusinessDelegate:createReferrerGroup() - Unable to create ReferrerGroup" + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferrerGroupDAO( dao );
        }        
        
        return( businessObject );
        
    }

   /**
    * Saves the underlying BO.
    * @param		businessObject		ReferrerGroup
    * @return       what was just saved
    * @exception    ProcessingException
    * @exception  	IllegalArgumentException
    */
    public ReferrerGroup saveReferrerGroup( ReferrerGroup businessObject ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "ReferrerGroupBusinessDelegate:saveReferrerGroup - ";
    	
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
                
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ReferrerGroupPrimaryKey key = businessObject.getReferrerGroupPrimaryKey();
                    
        if ( key != null )
        {
            ReferrerGroupDAO dao = getReferrerGroupDAO();

            try
            {                    
                businessObject = (ReferrerGroup)dao.saveReferrerGroup( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "ReferrerGroupBusinessDelegate:saveReferrerGroup() - Unable to save ReferrerGroup" + exc;
                LOGGER.warning( errMsg );
                throw new ProcessingException( errMsg  );
            }
            finally
            {
                releaseReferrerGroupDAO( dao );
            }
            
        }
        else
        {
            String errMsg = "ReferrerGroupBusinessDelegate:saveReferrerGroup() - Unable to create ReferrerGroup due to it having a null ReferrerGroupPrimaryKey."; 
            
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
		        
        return( businessObject );
        
    }
   
   /**
    * Deletes the associatied value object using the provided primary key.
    * @param		key 	ReferrerGroupPrimaryKey    
    * @exception 	ProcessingException
    */
    public void delete( ReferrerGroupPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {    	
    	String msgPrefix = "ReferrerGroupBusinessDelegate:saveReferrerGroup - ";
    	
		if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        ReferrerGroupDAO dao  = getReferrerGroupDAO();

        try
        {                    
            dao.deleteReferrerGroup( key );
        }
        catch (Exception exc)
        {
            String errMsg = msgPrefix + "Unable to delete ReferrerGroup using key = "  + key + ". " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferrerGroupDAO( dao );
        }
        		
        return;
    }
        

// business methods
    /**
     * Returns the ReferrerGroup specific DAO.
     *
     * @return      ReferrerGroup DAO
     */
    public ReferrerGroupDAO getReferrerGroupDAO()
    {
        return( new com.occulue.dao.ReferrerGroupDAO() ); 
    }

    /**
     * Release the ReferrerGroupDAO back to the FrameworkDAOFactory
     */
    public void releaseReferrerGroupDAO( com.occulue.dao.ReferrerGroupDAO dao )
    {
        dao = null;
    }

// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
// ~AIB
     
//************************************************************************
// Attributes
//************************************************************************

   /**
    * Singleton instance
    */
    protected static ReferrerGroupBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(ReferrerGroup.class.getName());
    
}



