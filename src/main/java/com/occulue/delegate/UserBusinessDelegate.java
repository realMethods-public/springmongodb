/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.*;

/**
 * User business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of User related services in the case of a User business related service failing.</li>
 * <li>Exposes a simpler, uniform User interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill User business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class UserBusinessDelegate 
extends ReferrerBusinessDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public UserBusinessDelegate() 
	{
	}

        
   /**
	* User Business Delegate Factory Method
	*
	* Returns a singleton instance of UserBusinessDelegate().
	* All methods are expected to be self-sufficient.
	*
	* @return 	UserBusinessDelegate
	*/
	public static UserBusinessDelegate getUserInstance()
	{
	    if ( singleton == null )
	    {
	    	singleton = new UserBusinessDelegate();
	    }
	    
	    return( singleton );
	}
 
    /**
     * Method to retrieve the User via an UserPrimaryKey.
     * @param 	key
     * @return 	User
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException 
     */
    public User getUser( UserPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "UserBusinessDelegate:getUser - ";
        if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        User returnBO = null;
                
        UserDAO dao = getUserDAO();
        
        try
        {
            returnBO = dao.findUser( key );
        }
        catch( Exception exc )
        {
            String errMsg = "UserBusinessDelegate:getUser( UserPrimaryKey key ) - unable to locate User with key " + key.toString() + " - " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseUserDAO( dao );
        }        
        
        return returnBO;
    }


    /**
     * Method to retrieve a collection of all Users
     *
     * @return 	ArrayList<User> 
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<User> getAllUser() 
    throws ProcessingException
    {
    	String msgPrefix				= "UserBusinessDelegate:getAllUser() - ";
        ArrayList<User> array	= null;
        
        UserDAO dao = getUserDAO();
    
        try
        {
            array = dao.findAllUser();
        }
        catch( Exception exc )
        {
            String errMsg = msgPrefix + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseUserDAO( dao );
        }        
        
        return array;
    }

   /**
    * Creates the provided BO.
    * @param		businessObject 	User
    * @return       User
    * @exception    ProcessingException
    * @exception	IllegalArgumentException
    */
	public User createUser( User businessObject )
    throws ProcessingException, IllegalArgumentException
    {
		String msgPrefix = "UserBusinessDelegate:createUser - ";
		
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        // return value once persisted
        UserDAO dao  = getUserDAO();
        
        try
        {
            businessObject = dao.createUser( businessObject );
        }
        catch (Exception exc)
        {
            String errMsg = "UserBusinessDelegate:createUser() - Unable to create User" + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseUserDAO( dao );
        }        
        
        return( businessObject );
        
    }

   /**
    * Saves the underlying BO.
    * @param		businessObject		User
    * @return       what was just saved
    * @exception    ProcessingException
    * @exception  	IllegalArgumentException
    */
    public User saveUser( User businessObject ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "UserBusinessDelegate:saveUser - ";
    	
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
                
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        UserPrimaryKey key = businessObject.getUserPrimaryKey();
                    
        if ( key != null )
        {
            UserDAO dao = getUserDAO();

            try
            {                    
                businessObject = (User)dao.saveUser( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "UserBusinessDelegate:saveUser() - Unable to save User" + exc;
                LOGGER.warning( errMsg );
                throw new ProcessingException( errMsg  );
            }
            finally
            {
                releaseUserDAO( dao );
            }
            
        }
        else
        {
            String errMsg = "UserBusinessDelegate:saveUser() - Unable to create User due to it having a null UserPrimaryKey."; 
            
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
		        
        return( businessObject );
        
    }
   
   /**
    * Deletes the associatied value object using the provided primary key.
    * @param		key 	UserPrimaryKey    
    * @exception 	ProcessingException
    */
    public void delete( UserPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {    	
    	String msgPrefix = "UserBusinessDelegate:saveUser - ";
    	
		if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        UserDAO dao  = getUserDAO();

        try
        {                    
            dao.deleteUser( key );
        }
        catch (Exception exc)
        {
            String errMsg = msgPrefix + "Unable to delete User using key = "  + key + ". " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseUserDAO( dao );
        }
        		
        return;
    }
        

// business methods
    /**
     * Returns the User specific DAO.
     *
     * @return      User DAO
     */
    public UserDAO getUserDAO()
    {
        return( new com.occulue.dao.UserDAO() ); 
    }

    /**
     * Release the UserDAO back to the FrameworkDAOFactory
     */
    public void releaseUserDAO( com.occulue.dao.UserDAO dao )
    {
        dao = null;
    }

// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
// ~AIB
     
//************************************************************************
// Attributes
//************************************************************************

   /**
    * Singleton instance
    */
    protected static UserBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(User.class.getName());
    
}



