/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.*;

/**
 * Answer business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Answer related services in the case of a Answer business related service failing.</li>
 * <li>Exposes a simpler, uniform Answer interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Answer business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class AnswerBusinessDelegate 
extends BaseBusinessDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public AnswerBusinessDelegate() 
	{
	}

        
   /**
	* Answer Business Delegate Factory Method
	*
	* Returns a singleton instance of AnswerBusinessDelegate().
	* All methods are expected to be self-sufficient.
	*
	* @return 	AnswerBusinessDelegate
	*/
	public static AnswerBusinessDelegate getAnswerInstance()
	{
	    if ( singleton == null )
	    {
	    	singleton = new AnswerBusinessDelegate();
	    }
	    
	    return( singleton );
	}
 
    /**
     * Method to retrieve the Answer via an AnswerPrimaryKey.
     * @param 	key
     * @return 	Answer
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException 
     */
    public Answer getAnswer( AnswerPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "AnswerBusinessDelegate:getAnswer - ";
        if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        Answer returnBO = null;
                
        AnswerDAO dao = getAnswerDAO();
        
        try
        {
            returnBO = dao.findAnswer( key );
        }
        catch( Exception exc )
        {
            String errMsg = "AnswerBusinessDelegate:getAnswer( AnswerPrimaryKey key ) - unable to locate Answer with key " + key.toString() + " - " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseAnswerDAO( dao );
        }        
        
        return returnBO;
    }


    /**
     * Method to retrieve a collection of all Answers
     *
     * @return 	ArrayList<Answer> 
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<Answer> getAllAnswer() 
    throws ProcessingException
    {
    	String msgPrefix				= "AnswerBusinessDelegate:getAllAnswer() - ";
        ArrayList<Answer> array	= null;
        
        AnswerDAO dao = getAnswerDAO();
    
        try
        {
            array = dao.findAllAnswer();
        }
        catch( Exception exc )
        {
            String errMsg = msgPrefix + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseAnswerDAO( dao );
        }        
        
        return array;
    }

   /**
    * Creates the provided BO.
    * @param		businessObject 	Answer
    * @return       Answer
    * @exception    ProcessingException
    * @exception	IllegalArgumentException
    */
	public Answer createAnswer( Answer businessObject )
    throws ProcessingException, IllegalArgumentException
    {
		String msgPrefix = "AnswerBusinessDelegate:createAnswer - ";
		
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        // return value once persisted
        AnswerDAO dao  = getAnswerDAO();
        
        try
        {
            businessObject = dao.createAnswer( businessObject );
        }
        catch (Exception exc)
        {
            String errMsg = "AnswerBusinessDelegate:createAnswer() - Unable to create Answer" + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseAnswerDAO( dao );
        }        
        
        return( businessObject );
        
    }

   /**
    * Saves the underlying BO.
    * @param		businessObject		Answer
    * @return       what was just saved
    * @exception    ProcessingException
    * @exception  	IllegalArgumentException
    */
    public Answer saveAnswer( Answer businessObject ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "AnswerBusinessDelegate:saveAnswer - ";
    	
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
                
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        AnswerPrimaryKey key = businessObject.getAnswerPrimaryKey();
                    
        if ( key != null )
        {
            AnswerDAO dao = getAnswerDAO();

            try
            {                    
                businessObject = (Answer)dao.saveAnswer( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "AnswerBusinessDelegate:saveAnswer() - Unable to save Answer" + exc;
                LOGGER.warning( errMsg );
                throw new ProcessingException( errMsg  );
            }
            finally
            {
                releaseAnswerDAO( dao );
            }
            
        }
        else
        {
            String errMsg = "AnswerBusinessDelegate:saveAnswer() - Unable to create Answer due to it having a null AnswerPrimaryKey."; 
            
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
		        
        return( businessObject );
        
    }
   
   /**
    * Deletes the associatied value object using the provided primary key.
    * @param		key 	AnswerPrimaryKey    
    * @exception 	ProcessingException
    */
    public void delete( AnswerPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {    	
    	String msgPrefix = "AnswerBusinessDelegate:saveAnswer - ";
    	
		if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        AnswerDAO dao  = getAnswerDAO();

        try
        {                    
            dao.deleteAnswer( key );
        }
        catch (Exception exc)
        {
            String errMsg = msgPrefix + "Unable to delete Answer using key = "  + key + ". " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseAnswerDAO( dao );
        }
        		
        return;
    }
        

// business methods
    /**
     * Returns the Answer specific DAO.
     *
     * @return      Answer DAO
     */
    public AnswerDAO getAnswerDAO()
    {
        return( new com.occulue.dao.AnswerDAO() ); 
    }

    /**
     * Release the AnswerDAO back to the FrameworkDAOFactory
     */
    public void releaseAnswerDAO( com.occulue.dao.AnswerDAO dao )
    {
        dao = null;
    }

// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
// ~AIB
     
//************************************************************************
// Attributes
//************************************************************************

   /**
    * Singleton instance
    */
    protected static AnswerBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(Answer.class.getName());
    
}



