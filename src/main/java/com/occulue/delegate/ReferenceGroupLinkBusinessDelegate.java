/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.*;

/**
 * ReferenceGroupLink business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of ReferenceGroupLink related services in the case of a ReferenceGroupLink business related service failing.</li>
 * <li>Exposes a simpler, uniform ReferenceGroupLink interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill ReferenceGroupLink business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class ReferenceGroupLinkBusinessDelegate 
extends BaseBusinessDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public ReferenceGroupLinkBusinessDelegate() 
	{
	}

        
   /**
	* ReferenceGroupLink Business Delegate Factory Method
	*
	* Returns a singleton instance of ReferenceGroupLinkBusinessDelegate().
	* All methods are expected to be self-sufficient.
	*
	* @return 	ReferenceGroupLinkBusinessDelegate
	*/
	public static ReferenceGroupLinkBusinessDelegate getReferenceGroupLinkInstance()
	{
	    if ( singleton == null )
	    {
	    	singleton = new ReferenceGroupLinkBusinessDelegate();
	    }
	    
	    return( singleton );
	}
 
    /**
     * Method to retrieve the ReferenceGroupLink via an ReferenceGroupLinkPrimaryKey.
     * @param 	key
     * @return 	ReferenceGroupLink
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException 
     */
    public ReferenceGroupLink getReferenceGroupLink( ReferenceGroupLinkPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "ReferenceGroupLinkBusinessDelegate:getReferenceGroupLink - ";
        if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        ReferenceGroupLink returnBO = null;
                
        ReferenceGroupLinkDAO dao = getReferenceGroupLinkDAO();
        
        try
        {
            returnBO = dao.findReferenceGroupLink( key );
        }
        catch( Exception exc )
        {
            String errMsg = "ReferenceGroupLinkBusinessDelegate:getReferenceGroupLink( ReferenceGroupLinkPrimaryKey key ) - unable to locate ReferenceGroupLink with key " + key.toString() + " - " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferenceGroupLinkDAO( dao );
        }        
        
        return returnBO;
    }


    /**
     * Method to retrieve a collection of all ReferenceGroupLinks
     *
     * @return 	ArrayList<ReferenceGroupLink> 
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<ReferenceGroupLink> getAllReferenceGroupLink() 
    throws ProcessingException
    {
    	String msgPrefix				= "ReferenceGroupLinkBusinessDelegate:getAllReferenceGroupLink() - ";
        ArrayList<ReferenceGroupLink> array	= null;
        
        ReferenceGroupLinkDAO dao = getReferenceGroupLinkDAO();
    
        try
        {
            array = dao.findAllReferenceGroupLink();
        }
        catch( Exception exc )
        {
            String errMsg = msgPrefix + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferenceGroupLinkDAO( dao );
        }        
        
        return array;
    }

   /**
    * Creates the provided BO.
    * @param		businessObject 	ReferenceGroupLink
    * @return       ReferenceGroupLink
    * @exception    ProcessingException
    * @exception	IllegalArgumentException
    */
	public ReferenceGroupLink createReferenceGroupLink( ReferenceGroupLink businessObject )
    throws ProcessingException, IllegalArgumentException
    {
		String msgPrefix = "ReferenceGroupLinkBusinessDelegate:createReferenceGroupLink - ";
		
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        // return value once persisted
        ReferenceGroupLinkDAO dao  = getReferenceGroupLinkDAO();
        
        try
        {
            businessObject = dao.createReferenceGroupLink( businessObject );
        }
        catch (Exception exc)
        {
            String errMsg = "ReferenceGroupLinkBusinessDelegate:createReferenceGroupLink() - Unable to create ReferenceGroupLink" + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferenceGroupLinkDAO( dao );
        }        
        
        return( businessObject );
        
    }

   /**
    * Saves the underlying BO.
    * @param		businessObject		ReferenceGroupLink
    * @return       what was just saved
    * @exception    ProcessingException
    * @exception  	IllegalArgumentException
    */
    public ReferenceGroupLink saveReferenceGroupLink( ReferenceGroupLink businessObject ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "ReferenceGroupLinkBusinessDelegate:saveReferenceGroupLink - ";
    	
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
                
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ReferenceGroupLinkPrimaryKey key = businessObject.getReferenceGroupLinkPrimaryKey();
                    
        if ( key != null )
        {
            ReferenceGroupLinkDAO dao = getReferenceGroupLinkDAO();

            try
            {                    
                businessObject = (ReferenceGroupLink)dao.saveReferenceGroupLink( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "ReferenceGroupLinkBusinessDelegate:saveReferenceGroupLink() - Unable to save ReferenceGroupLink" + exc;
                LOGGER.warning( errMsg );
                throw new ProcessingException( errMsg  );
            }
            finally
            {
                releaseReferenceGroupLinkDAO( dao );
            }
            
        }
        else
        {
            String errMsg = "ReferenceGroupLinkBusinessDelegate:saveReferenceGroupLink() - Unable to create ReferenceGroupLink due to it having a null ReferenceGroupLinkPrimaryKey."; 
            
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
		        
        return( businessObject );
        
    }
   
   /**
    * Deletes the associatied value object using the provided primary key.
    * @param		key 	ReferenceGroupLinkPrimaryKey    
    * @exception 	ProcessingException
    */
    public void delete( ReferenceGroupLinkPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {    	
    	String msgPrefix = "ReferenceGroupLinkBusinessDelegate:saveReferenceGroupLink - ";
    	
		if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        ReferenceGroupLinkDAO dao  = getReferenceGroupLinkDAO();

        try
        {                    
            dao.deleteReferenceGroupLink( key );
        }
        catch (Exception exc)
        {
            String errMsg = msgPrefix + "Unable to delete ReferenceGroupLink using key = "  + key + ". " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferenceGroupLinkDAO( dao );
        }
        		
        return;
    }
        

// business methods
    /**
     * Returns the ReferenceGroupLink specific DAO.
     *
     * @return      ReferenceGroupLink DAO
     */
    public ReferenceGroupLinkDAO getReferenceGroupLinkDAO()
    {
        return( new com.occulue.dao.ReferenceGroupLinkDAO() ); 
    }

    /**
     * Release the ReferenceGroupLinkDAO back to the FrameworkDAOFactory
     */
    public void releaseReferenceGroupLinkDAO( com.occulue.dao.ReferenceGroupLinkDAO dao )
    {
        dao = null;
    }

// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
// ~AIB
     
//************************************************************************
// Attributes
//************************************************************************

   /**
    * Singleton instance
    */
    protected static ReferenceGroupLinkBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(ReferenceGroupLink.class.getName());
    
}



