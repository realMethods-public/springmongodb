/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.*;

/**
 * Referrer business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Referrer related services in the case of a Referrer business related service failing.</li>
 * <li>Exposes a simpler, uniform Referrer interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Referrer business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class ReferrerBusinessDelegate 
extends BaseBusinessDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public ReferrerBusinessDelegate() 
	{
	}

        
   /**
	* Referrer Business Delegate Factory Method
	*
	* Returns a singleton instance of ReferrerBusinessDelegate().
	* All methods are expected to be self-sufficient.
	*
	* @return 	ReferrerBusinessDelegate
	*/
	public static ReferrerBusinessDelegate getReferrerInstance()
	{
	    if ( singleton == null )
	    {
	    	singleton = new ReferrerBusinessDelegate();
	    }
	    
	    return( singleton );
	}
 
    /**
     * Method to retrieve the Referrer via an ReferrerPrimaryKey.
     * @param 	key
     * @return 	Referrer
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException 
     */
    public Referrer getReferrer( ReferrerPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "ReferrerBusinessDelegate:getReferrer - ";
        if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        Referrer returnBO = null;
                
        ReferrerDAO dao = getReferrerDAO();
        
        try
        {
            returnBO = dao.findReferrer( key );
        }
        catch( Exception exc )
        {
            String errMsg = "ReferrerBusinessDelegate:getReferrer( ReferrerPrimaryKey key ) - unable to locate Referrer with key " + key.toString() + " - " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferrerDAO( dao );
        }        
        
        return returnBO;
    }


    /**
     * Method to retrieve a collection of all Referrers
     *
     * @return 	ArrayList<Referrer> 
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<Referrer> getAllReferrer() 
    throws ProcessingException
    {
    	String msgPrefix				= "ReferrerBusinessDelegate:getAllReferrer() - ";
        ArrayList<Referrer> array	= null;
        
        ReferrerDAO dao = getReferrerDAO();
    
        try
        {
            array = dao.findAllReferrer();
        }
        catch( Exception exc )
        {
            String errMsg = msgPrefix + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferrerDAO( dao );
        }        
        
        return array;
    }

   /**
    * Creates the provided BO.
    * @param		businessObject 	Referrer
    * @return       Referrer
    * @exception    ProcessingException
    * @exception	IllegalArgumentException
    */
	public Referrer createReferrer( Referrer businessObject )
    throws ProcessingException, IllegalArgumentException
    {
		String msgPrefix = "ReferrerBusinessDelegate:createReferrer - ";
		
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        // return value once persisted
        ReferrerDAO dao  = getReferrerDAO();
        
        try
        {
            businessObject = dao.createReferrer( businessObject );
        }
        catch (Exception exc)
        {
            String errMsg = "ReferrerBusinessDelegate:createReferrer() - Unable to create Referrer" + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferrerDAO( dao );
        }        
        
        return( businessObject );
        
    }

   /**
    * Saves the underlying BO.
    * @param		businessObject		Referrer
    * @return       what was just saved
    * @exception    ProcessingException
    * @exception  	IllegalArgumentException
    */
    public Referrer saveReferrer( Referrer businessObject ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "ReferrerBusinessDelegate:saveReferrer - ";
    	
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
                
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ReferrerPrimaryKey key = businessObject.getReferrerPrimaryKey();
                    
        if ( key != null )
        {
            ReferrerDAO dao = getReferrerDAO();

            try
            {                    
                businessObject = (Referrer)dao.saveReferrer( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "ReferrerBusinessDelegate:saveReferrer() - Unable to save Referrer" + exc;
                LOGGER.warning( errMsg );
                throw new ProcessingException( errMsg  );
            }
            finally
            {
                releaseReferrerDAO( dao );
            }
            
        }
        else
        {
            String errMsg = "ReferrerBusinessDelegate:saveReferrer() - Unable to create Referrer due to it having a null ReferrerPrimaryKey."; 
            
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
		        
        return( businessObject );
        
    }
   
   /**
    * Deletes the associatied value object using the provided primary key.
    * @param		key 	ReferrerPrimaryKey    
    * @exception 	ProcessingException
    */
    public void delete( ReferrerPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {    	
    	String msgPrefix = "ReferrerBusinessDelegate:saveReferrer - ";
    	
		if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        ReferrerDAO dao  = getReferrerDAO();

        try
        {                    
            dao.deleteReferrer( key );
        }
        catch (Exception exc)
        {
            String errMsg = msgPrefix + "Unable to delete Referrer using key = "  + key + ". " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseReferrerDAO( dao );
        }
        		
        return;
    }
        

// business methods
    /**
     * Returns the Referrer specific DAO.
     *
     * @return      Referrer DAO
     */
    public ReferrerDAO getReferrerDAO()
    {
        return( new com.occulue.dao.ReferrerDAO() ); 
    }

    /**
     * Release the ReferrerDAO back to the FrameworkDAOFactory
     */
    public void releaseReferrerDAO( com.occulue.dao.ReferrerDAO dao )
    {
        dao = null;
    }

// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
// ~AIB
     
//************************************************************************
// Attributes
//************************************************************************

   /**
    * Singleton instance
    */
    protected static ReferrerBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(Referrer.class.getName());
    
}



