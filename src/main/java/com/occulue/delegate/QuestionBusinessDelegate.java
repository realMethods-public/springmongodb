/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.util.HashMap;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.*;

/**
 * Question business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Question related services in the case of a Question business related service failing.</li>
 * <li>Exposes a simpler, uniform Question interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Question business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class QuestionBusinessDelegate 
extends BaseBusinessDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public QuestionBusinessDelegate() 
	{
	}

        
   /**
	* Question Business Delegate Factory Method
	*
	* Returns a singleton instance of QuestionBusinessDelegate().
	* All methods are expected to be self-sufficient.
	*
	* @return 	QuestionBusinessDelegate
	*/
	public static QuestionBusinessDelegate getQuestionInstance()
	{
	    if ( singleton == null )
	    {
	    	singleton = new QuestionBusinessDelegate();
	    }
	    
	    return( singleton );
	}
 
    /**
     * Method to retrieve the Question via an QuestionPrimaryKey.
     * @param 	key
     * @return 	Question
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException 
     */
    public Question getQuestion( QuestionPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "QuestionBusinessDelegate:getQuestion - ";
        if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        Question returnBO = null;
                
        QuestionDAO dao = getQuestionDAO();
        
        try
        {
            returnBO = dao.findQuestion( key );
        }
        catch( Exception exc )
        {
            String errMsg = "QuestionBusinessDelegate:getQuestion( QuestionPrimaryKey key ) - unable to locate Question with key " + key.toString() + " - " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseQuestionDAO( dao );
        }        
        
        return returnBO;
    }


    /**
     * Method to retrieve a collection of all Questions
     *
     * @return 	ArrayList<Question> 
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<Question> getAllQuestion() 
    throws ProcessingException
    {
    	String msgPrefix				= "QuestionBusinessDelegate:getAllQuestion() - ";
        ArrayList<Question> array	= null;
        
        QuestionDAO dao = getQuestionDAO();
    
        try
        {
            array = dao.findAllQuestion();
        }
        catch( Exception exc )
        {
            String errMsg = msgPrefix + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseQuestionDAO( dao );
        }        
        
        return array;
    }

   /**
    * Creates the provided BO.
    * @param		businessObject 	Question
    * @return       Question
    * @exception    ProcessingException
    * @exception	IllegalArgumentException
    */
	public Question createQuestion( Question businessObject )
    throws ProcessingException, IllegalArgumentException
    {
		String msgPrefix = "QuestionBusinessDelegate:createQuestion - ";
		
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        // return value once persisted
        QuestionDAO dao  = getQuestionDAO();
        
        try
        {
            businessObject = dao.createQuestion( businessObject );
        }
        catch (Exception exc)
        {
            String errMsg = "QuestionBusinessDelegate:createQuestion() - Unable to create Question" + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseQuestionDAO( dao );
        }        
        
        return( businessObject );
        
    }

   /**
    * Saves the underlying BO.
    * @param		businessObject		Question
    * @return       what was just saved
    * @exception    ProcessingException
    * @exception  	IllegalArgumentException
    */
    public Question saveQuestion( Question businessObject ) 
    throws ProcessingException, IllegalArgumentException
    {
    	String msgPrefix = "QuestionBusinessDelegate:saveQuestion - ";
    	
		if ( businessObject == null )
        {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
                
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        QuestionPrimaryKey key = businessObject.getQuestionPrimaryKey();
                    
        if ( key != null )
        {
            QuestionDAO dao = getQuestionDAO();

            try
            {                    
                businessObject = (Question)dao.saveQuestion( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "QuestionBusinessDelegate:saveQuestion() - Unable to save Question" + exc;
                LOGGER.warning( errMsg );
                throw new ProcessingException( errMsg  );
            }
            finally
            {
                releaseQuestionDAO( dao );
            }
            
        }
        else
        {
            String errMsg = "QuestionBusinessDelegate:saveQuestion() - Unable to create Question due to it having a null QuestionPrimaryKey."; 
            
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
		        
        return( businessObject );
        
    }
   
   /**
    * Deletes the associatied value object using the provided primary key.
    * @param		key 	QuestionPrimaryKey    
    * @exception 	ProcessingException
    */
    public void delete( QuestionPrimaryKey key ) 
    throws ProcessingException, IllegalArgumentException
    {    	
    	String msgPrefix = "QuestionBusinessDelegate:saveQuestion - ";
    	
		if ( key == null )
        {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning( errMsg );
            throw new IllegalArgumentException( errMsg );
        }
        
        QuestionDAO dao  = getQuestionDAO();

        try
        {                    
            dao.deleteQuestion( key );
        }
        catch (Exception exc)
        {
            String errMsg = msgPrefix + "Unable to delete Question using key = "  + key + ". " + exc;
            LOGGER.warning( errMsg );
            throw new ProcessingException( errMsg );
        }
        finally
        {
            releaseQuestionDAO( dao );
        }
        		
        return;
    }
        

// business methods
    /**
     * Returns the Question specific DAO.
     *
     * @return      Question DAO
     */
    public QuestionDAO getQuestionDAO()
    {
        return( new com.occulue.dao.QuestionDAO() ); 
    }

    /**
     * Release the QuestionDAO back to the FrameworkDAOFactory
     */
    public void releaseQuestionDAO( com.occulue.dao.QuestionDAO dao )
    {
        dao = null;
    }

// AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
// ~AIB
     
//************************************************************************
// Attributes
//************************************************************************

   /**
    * Singleton instance
    */
    protected static QuestionBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(Question.class.getName());
    
}



