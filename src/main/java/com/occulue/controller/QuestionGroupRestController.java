/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity QuestionGroup.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/QuestionGroup")
public class QuestionGroupRestController extends BaseSpringRestController
{

    /**
     * Handles saving a QuestionGroup BO.  if not key provided, calls create, otherwise calls save
     * @param		QuestionGroup questionGroup
     * @return		QuestionGroup
     */
	@RequestMapping("/save")
    public QuestionGroup save( @RequestBody QuestionGroup questionGroup )
    {
    	// assign locally
    	this.questionGroup = questionGroup;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a QuestionGroup BO
     * @param		Long questionGroupId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="questionGroup.questionGroupId", required=false) Long questionGroupId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	QuestionGroupBusinessDelegate delegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new QuestionGroupPrimaryKey( questionGroupId ) );
        		LOGGER.info( "QuestionGroupController:delete() - successfully deleted QuestionGroup with key " + questionGroup.getQuestionGroupPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new QuestionGroupPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "QuestionGroupController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "QuestionGroupController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a QuestionGroup BO
     * @param		Long questionGroupId
     * @return		QuestionGroup
     */    
    @RequestMapping("/load")
    public QuestionGroup load( @RequestParam(value="questionGroup.questionGroupId", required=true) Long questionGroupId )
    {    	
        QuestionGroupPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****QuestionGroup.load pk is " + questionGroupId );
        	if ( questionGroupId != null )
        	{
        		pk = new QuestionGroupPrimaryKey( questionGroupId );
        		
        		// load the QuestionGroup
	            this.questionGroup = QuestionGroupBusinessDelegate.getQuestionGroupInstance().getQuestionGroup( pk );
	            
	            LOGGER.info( "QuestionGroupController:load() - successfully loaded - " + this.questionGroup.toString() );             
			}
			else
			{
	            LOGGER.info( "QuestionGroupController:load() - unable to locate the primary key as an attribute or a selection for - " + questionGroup.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "QuestionGroupController:load() - failed to load QuestionGroup using Id " + questionGroupId + ", " + exc.getMessage() );
            return null;
        }

        return questionGroup;

    }

    /**
     * Handles loading all QuestionGroup business objects
     * @return		List<QuestionGroup>
     */
    @RequestMapping("/loadAll")
    public List<QuestionGroup> loadAll()
    {                
        List<QuestionGroup> questionGroupList = null;
        
    	try
        {                        
            // load the QuestionGroup
            questionGroupList = QuestionGroupBusinessDelegate.getQuestionGroupInstance().getAllQuestionGroup();
            
            if ( questionGroupList != null )
                LOGGER.info(  "QuestionGroupController:loadAllQuestionGroup() - successfully loaded all QuestionGroups" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "QuestionGroupController:loadAll() - failed to load all QuestionGroups - " + exc.getMessage() );
        	return null;
            
        }

        return questionGroupList;
                            
    }


// findAllBy methods



    /**
     * save Questions on QuestionGroup
     * @param		Long questionGroupId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		QuestionGroup
     */     
	@RequestMapping("/saveQuestions")
	public QuestionGroup saveQuestions( @RequestParam(value="questionGroup.questionGroupId", required=false) Long questionGroupId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( questionGroupId ) == null )
			return( null );
		
		QuestionPrimaryKey pk 					= null;
		Question child							= null;
		QuestionBusinessDelegate childDelegate 	= QuestionBusinessDelegate.getQuestionInstance();
		QuestionGroupBusinessDelegate parentDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new QuestionPrimaryKey( childId );
			
			try
			{
				// find the Question
				child = childDelegate.getQuestion( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "QuestionGroupController:saveQuestions() failed get child Question using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the Questions 
			questionGroup.getQuestions().add( child );				
		}
		else
		{
			// clear out the Questions but 
			questionGroup.getQuestions().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new QuestionPrimaryKey( id );
					try
					{
						// find the Question
						child = childDelegate.getQuestion( pk );
						// add it to the Questions List
						questionGroup.getQuestions().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "QuestionGroupController:saveQuestions() failed get child Question using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the QuestionGroup
			parentDelegate.saveQuestionGroup( questionGroup );
		}
		catch( Exception exc )
		{
			LOGGER.info( "QuestionGroupController:saveQuestions() failed saving parent QuestionGroup - " + exc.getMessage() );
		}

		return questionGroup;
	}

    /**
     * delete Questions on QuestionGroup
     * @param		Long questionGroupId
     * @param		Long[] childIds
     * @return		QuestionGroup
     */     	
	@RequestMapping("/deleteQuestions")
	public QuestionGroup deleteQuestions( @RequestParam(value="questionGroup.questionGroupId", required=true) Long questionGroupId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( questionGroupId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			QuestionPrimaryKey pk 					= null;
			QuestionBusinessDelegate childDelegate 	= QuestionBusinessDelegate.getQuestionInstance();
			QuestionGroupBusinessDelegate parentDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
			Set<Question> children					= questionGroup.getQuestions();
			Question child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new QuestionPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getQuestion( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "QuestionGroupController:deleteQuestions() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of Question back to the questionGroup
			questionGroup.setQuestions( children );			
			// save it 
			try
			{
				questionGroup = parentDelegate.saveQuestionGroup( questionGroup );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "QuestionGroupController:deleteQuestions() failed to save the QuestionGroup - " + exc.getMessage() );
			}
		}
		
		return questionGroup;
	}

    /**
     * save QuestionGroups on QuestionGroup
     * @param		Long questionGroupId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		QuestionGroup
     */     
	@RequestMapping("/saveQuestionGroups")
	public QuestionGroup saveQuestionGroups( @RequestParam(value="questionGroup.questionGroupId", required=false) Long questionGroupId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( questionGroupId ) == null )
			return( null );
		
		QuestionGroupPrimaryKey pk 					= null;
		QuestionGroup child							= null;
		QuestionGroupBusinessDelegate childDelegate 	= QuestionGroupBusinessDelegate.getQuestionGroupInstance();
		QuestionGroupBusinessDelegate parentDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new QuestionGroupPrimaryKey( childId );
			
			try
			{
				// find the QuestionGroup
				child = childDelegate.getQuestionGroup( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "QuestionGroupController:saveQuestionGroups() failed get child QuestionGroup using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the QuestionGroups 
			questionGroup.getQuestionGroups().add( child );				
		}
		else
		{
			// clear out the QuestionGroups but 
			questionGroup.getQuestionGroups().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new QuestionGroupPrimaryKey( id );
					try
					{
						// find the QuestionGroup
						child = childDelegate.getQuestionGroup( pk );
						// add it to the QuestionGroups List
						questionGroup.getQuestionGroups().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "QuestionGroupController:saveQuestionGroups() failed get child QuestionGroup using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the QuestionGroup
			parentDelegate.saveQuestionGroup( questionGroup );
		}
		catch( Exception exc )
		{
			LOGGER.info( "QuestionGroupController:saveQuestionGroups() failed saving parent QuestionGroup - " + exc.getMessage() );
		}

		return questionGroup;
	}

    /**
     * delete QuestionGroups on QuestionGroup
     * @param		Long questionGroupId
     * @param		Long[] childIds
     * @return		QuestionGroup
     */     	
	@RequestMapping("/deleteQuestionGroups")
	public QuestionGroup deleteQuestionGroups( @RequestParam(value="questionGroup.questionGroupId", required=true) Long questionGroupId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( questionGroupId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			QuestionGroupPrimaryKey pk 					= null;
			QuestionGroupBusinessDelegate childDelegate 	= QuestionGroupBusinessDelegate.getQuestionGroupInstance();
			QuestionGroupBusinessDelegate parentDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
			Set<QuestionGroup> children					= questionGroup.getQuestionGroups();
			QuestionGroup child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new QuestionGroupPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getQuestionGroup( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "QuestionGroupController:deleteQuestionGroups() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of QuestionGroup back to the questionGroup
			questionGroup.setQuestionGroups( children );			
			// save it 
			try
			{
				questionGroup = parentDelegate.saveQuestionGroup( questionGroup );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "QuestionGroupController:deleteQuestionGroups() failed to save the QuestionGroup - " + exc.getMessage() );
			}
		}
		
		return questionGroup;
	}


    /**
     * Handles creating a QuestionGroup BO
     * @return		QuestionGroup
     */
    protected QuestionGroup create()
    {
        try
        {       
			this.questionGroup = QuestionGroupBusinessDelegate.getQuestionGroupInstance().createQuestionGroup( questionGroup );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "QuestionGroupController:create() - exception QuestionGroup - " + exc.getMessage());        	
        	return null;
        }
        
        return this.questionGroup;
    }

    /**
     * Handles updating a QuestionGroup BO
     * @return		QuestionGroup
     */    
    protected QuestionGroup update()
    {
    	// store provided data
        QuestionGroup tmp = questionGroup;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	questionGroup.copyShallow( tmp );
    	
        try
        {                        	        
			// create the QuestionGroupBusiness Delegate            
			QuestionGroupBusinessDelegate delegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
            this.questionGroup = delegate.saveQuestionGroup( questionGroup );
            
            if ( this.questionGroup != null )
                LOGGER.info( "QuestionGroupController:update() - successfully updated QuestionGroup - " + questionGroup.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "QuestionGroupController:update() - successfully update QuestionGroup - " + exc.getMessage());        	
        	return null;
        }
        
        return this.questionGroup;
        
    }


    /**
     * Returns true if the questionGroup is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( questionGroup != null && questionGroup.getQuestionGroupPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected QuestionGroup load()
    {
    	return( load( new Long( questionGroup.getQuestionGroupPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected QuestionGroup questionGroup = null;
    private static final Logger LOGGER = Logger.getLogger(QuestionGroup.class.getName());
    
}


