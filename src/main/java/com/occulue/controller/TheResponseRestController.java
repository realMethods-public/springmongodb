/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity TheResponse.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/TheResponse")
public class TheResponseRestController extends BaseSpringRestController
{

    /**
     * Handles saving a TheResponse BO.  if not key provided, calls create, otherwise calls save
     * @param		TheResponse theResponse
     * @return		TheResponse
     */
	@RequestMapping("/save")
    public TheResponse save( @RequestBody TheResponse theResponse )
    {
    	// assign locally
    	this.theResponse = theResponse;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a TheResponse BO
     * @param		Long theResponseId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="theResponse.theResponseId", required=false) Long theResponseId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	TheResponseBusinessDelegate delegate = TheResponseBusinessDelegate.getTheResponseInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new TheResponsePrimaryKey( theResponseId ) );
        		LOGGER.info( "TheResponseController:delete() - successfully deleted TheResponse with key " + theResponse.getTheResponsePrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new TheResponsePrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "TheResponseController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "TheResponseController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a TheResponse BO
     * @param		Long theResponseId
     * @return		TheResponse
     */    
    @RequestMapping("/load")
    public TheResponse load( @RequestParam(value="theResponse.theResponseId", required=true) Long theResponseId )
    {    	
        TheResponsePrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****TheResponse.load pk is " + theResponseId );
        	if ( theResponseId != null )
        	{
        		pk = new TheResponsePrimaryKey( theResponseId );
        		
        		// load the TheResponse
	            this.theResponse = TheResponseBusinessDelegate.getTheResponseInstance().getTheResponse( pk );
	            
	            LOGGER.info( "TheResponseController:load() - successfully loaded - " + this.theResponse.toString() );             
			}
			else
			{
	            LOGGER.info( "TheResponseController:load() - unable to locate the primary key as an attribute or a selection for - " + theResponse.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "TheResponseController:load() - failed to load TheResponse using Id " + theResponseId + ", " + exc.getMessage() );
            return null;
        }

        return theResponse;

    }

    /**
     * Handles loading all TheResponse business objects
     * @return		List<TheResponse>
     */
    @RequestMapping("/loadAll")
    public List<TheResponse> loadAll()
    {                
        List<TheResponse> theResponseList = null;
        
    	try
        {                        
            // load the TheResponse
            theResponseList = TheResponseBusinessDelegate.getTheResponseInstance().getAllTheResponse();
            
            if ( theResponseList != null )
                LOGGER.info(  "TheResponseController:loadAllTheResponse() - successfully loaded all TheResponses" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "TheResponseController:loadAll() - failed to load all TheResponses - " + exc.getMessage() );
        	return null;
            
        }

        return theResponseList;
                            
    }


// findAllBy methods




    /**
     * Handles creating a TheResponse BO
     * @return		TheResponse
     */
    protected TheResponse create()
    {
        try
        {       
			this.theResponse = TheResponseBusinessDelegate.getTheResponseInstance().createTheResponse( theResponse );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "TheResponseController:create() - exception TheResponse - " + exc.getMessage());        	
        	return null;
        }
        
        return this.theResponse;
    }

    /**
     * Handles updating a TheResponse BO
     * @return		TheResponse
     */    
    protected TheResponse update()
    {
    	// store provided data
        TheResponse tmp = theResponse;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	theResponse.copyShallow( tmp );
    	
        try
        {                        	        
			// create the TheResponseBusiness Delegate            
			TheResponseBusinessDelegate delegate = TheResponseBusinessDelegate.getTheResponseInstance();
            this.theResponse = delegate.saveTheResponse( theResponse );
            
            if ( this.theResponse != null )
                LOGGER.info( "TheResponseController:update() - successfully updated TheResponse - " + theResponse.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "TheResponseController:update() - successfully update TheResponse - " + exc.getMessage());        	
        	return null;
        }
        
        return this.theResponse;
        
    }


    /**
     * Returns true if the theResponse is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( theResponse != null && theResponse.getTheResponsePrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected TheResponse load()
    {
    	return( load( new Long( theResponse.getTheResponsePrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected TheResponse theResponse = null;
    private static final Logger LOGGER = Logger.getLogger(TheResponse.class.getName());
    
}


