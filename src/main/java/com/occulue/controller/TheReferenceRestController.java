/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity TheReference.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/TheReference")
public class TheReferenceRestController extends BaseSpringRestController
{

    /**
     * Handles saving a TheReference BO.  if not key provided, calls create, otherwise calls save
     * @param		TheReference theReference
     * @return		TheReference
     */
	@RequestMapping("/save")
    public TheReference save( @RequestBody TheReference theReference )
    {
    	// assign locally
    	this.theReference = theReference;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a TheReference BO
     * @param		Long theReferenceId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="theReference.theReferenceId", required=false) Long theReferenceId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	TheReferenceBusinessDelegate delegate = TheReferenceBusinessDelegate.getTheReferenceInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new TheReferencePrimaryKey( theReferenceId ) );
        		LOGGER.info( "TheReferenceController:delete() - successfully deleted TheReference with key " + theReference.getTheReferencePrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new TheReferencePrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "TheReferenceController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "TheReferenceController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a TheReference BO
     * @param		Long theReferenceId
     * @return		TheReference
     */    
    @RequestMapping("/load")
    public TheReference load( @RequestParam(value="theReference.theReferenceId", required=true) Long theReferenceId )
    {    	
        TheReferencePrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****TheReference.load pk is " + theReferenceId );
        	if ( theReferenceId != null )
        	{
        		pk = new TheReferencePrimaryKey( theReferenceId );
        		
        		// load the TheReference
	            this.theReference = TheReferenceBusinessDelegate.getTheReferenceInstance().getTheReference( pk );
	            
	            LOGGER.info( "TheReferenceController:load() - successfully loaded - " + this.theReference.toString() );             
			}
			else
			{
	            LOGGER.info( "TheReferenceController:load() - unable to locate the primary key as an attribute or a selection for - " + theReference.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "TheReferenceController:load() - failed to load TheReference using Id " + theReferenceId + ", " + exc.getMessage() );
            return null;
        }

        return theReference;

    }

    /**
     * Handles loading all TheReference business objects
     * @return		List<TheReference>
     */
    @RequestMapping("/loadAll")
    public List<TheReference> loadAll()
    {                
        List<TheReference> theReferenceList = null;
        
    	try
        {                        
            // load the TheReference
            theReferenceList = TheReferenceBusinessDelegate.getTheReferenceInstance().getAllTheReference();
            
            if ( theReferenceList != null )
                LOGGER.info(  "TheReferenceController:loadAllTheReference() - successfully loaded all TheReferences" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "TheReferenceController:loadAll() - failed to load all TheReferences - " + exc.getMessage() );
        	return null;
            
        }

        return theReferenceList;
                            
    }


// findAllBy methods


    /**
     * save QuestionGroup on TheReference
     * @param		Long	theReferenceId
     * @param		Long	childId
     * @param		TheReference theReference
     * @return		TheReference
     */     
	@RequestMapping("/saveQuestionGroup")
	public TheReference saveQuestionGroup( @RequestParam(value="theReference.theReferenceId", required=true) Long theReferenceId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody TheReference theReference )
	{
		if ( load( theReferenceId ) == null )
			return( null );
		
		if ( childId != null )
		{
			QuestionGroupBusinessDelegate childDelegate 	= QuestionGroupBusinessDelegate.getQuestionGroupInstance();
			TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();			
			QuestionGroup child 							= null;

			try
			{
				child = childDelegate.getQuestionGroup( new QuestionGroupPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "TheReferenceController:saveQuestionGroup() failed to get QuestionGroup using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			theReference.setQuestionGroup( child );
		
			try
			{
				// save it
				parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:saveQuestionGroup() failed saving parent TheReference - " + exc.getMessage() );
			}
		}
		
		return theReference;
	}

    /**
     * delete QuestionGroup on TheReference
     * @param		Long theReferenceId
     * @return		TheReference
     */     
	@RequestMapping("/deleteQuestionGroup")
	public TheReference deleteQuestionGroup( @RequestParam(value="theReference.theReferenceId", required=true) Long theReferenceId )
	
	{
		if ( load( theReferenceId ) == null )
			return( null );

		if ( theReference.getQuestionGroup() != null )
		{
			QuestionGroupPrimaryKey pk = theReference.getQuestionGroup().getQuestionGroupPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			theReference.setQuestionGroup( null );
			try
			{
				TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();

				// save it
				theReference = parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:deleteQuestionGroup() failed to save TheReference - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				QuestionGroupBusinessDelegate childDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:deleteQuestionGroup() failed  - " + exc.getMessage() );
			}
		}
		
		return theReference;
	}
	
    /**
     * save User on TheReference
     * @param		Long	theReferenceId
     * @param		Long	childId
     * @param		TheReference theReference
     * @return		TheReference
     */     
	@RequestMapping("/saveUser")
	public TheReference saveUser( @RequestParam(value="theReference.theReferenceId", required=true) Long theReferenceId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody TheReference theReference )
	{
		if ( load( theReferenceId ) == null )
			return( null );
		
		if ( childId != null )
		{
			UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
			TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();			
			User child 							= null;

			try
			{
				child = childDelegate.getUser( new UserPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "TheReferenceController:saveUser() failed to get User using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			theReference.setUser( child );
		
			try
			{
				// save it
				parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:saveUser() failed saving parent TheReference - " + exc.getMessage() );
			}
		}
		
		return theReference;
	}

    /**
     * delete User on TheReference
     * @param		Long theReferenceId
     * @return		TheReference
     */     
	@RequestMapping("/deleteUser")
	public TheReference deleteUser( @RequestParam(value="theReference.theReferenceId", required=true) Long theReferenceId )
	
	{
		if ( load( theReferenceId ) == null )
			return( null );

		if ( theReference.getUser() != null )
		{
			UserPrimaryKey pk = theReference.getUser().getUserPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			theReference.setUser( null );
			try
			{
				TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();

				// save it
				theReference = parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:deleteUser() failed to save TheReference - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				UserBusinessDelegate childDelegate = UserBusinessDelegate.getUserInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:deleteUser() failed  - " + exc.getMessage() );
			}
		}
		
		return theReference;
	}
	
    /**
     * save Referrer on TheReference
     * @param		Long	theReferenceId
     * @param		Long	childId
     * @param		TheReference theReference
     * @return		TheReference
     */     
	@RequestMapping("/saveReferrer")
	public TheReference saveReferrer( @RequestParam(value="theReference.theReferenceId", required=true) Long theReferenceId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody TheReference theReference )
	{
		if ( load( theReferenceId ) == null )
			return( null );
		
		if ( childId != null )
		{
			ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
			TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();			
			Referrer child 							= null;

			try
			{
				child = childDelegate.getReferrer( new ReferrerPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "TheReferenceController:saveReferrer() failed to get Referrer using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			theReference.setReferrer( child );
		
			try
			{
				// save it
				parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:saveReferrer() failed saving parent TheReference - " + exc.getMessage() );
			}
		}
		
		return theReference;
	}

    /**
     * delete Referrer on TheReference
     * @param		Long theReferenceId
     * @return		TheReference
     */     
	@RequestMapping("/deleteReferrer")
	public TheReference deleteReferrer( @RequestParam(value="theReference.theReferenceId", required=true) Long theReferenceId )
	
	{
		if ( load( theReferenceId ) == null )
			return( null );

		if ( theReference.getReferrer() != null )
		{
			ReferrerPrimaryKey pk = theReference.getReferrer().getReferrerPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			theReference.setReferrer( null );
			try
			{
				TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();

				// save it
				theReference = parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:deleteReferrer() failed to save TheReference - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				ReferrerBusinessDelegate childDelegate = ReferrerBusinessDelegate.getReferrerInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:deleteReferrer() failed  - " + exc.getMessage() );
			}
		}
		
		return theReference;
	}
	
    /**
     * save LastQuestionAnswered on TheReference
     * @param		Long	theReferenceId
     * @param		Long	childId
     * @param		TheReference theReference
     * @return		TheReference
     */     
	@RequestMapping("/saveLastQuestionAnswered")
	public TheReference saveLastQuestionAnswered( @RequestParam(value="theReference.theReferenceId", required=true) Long theReferenceId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody TheReference theReference )
	{
		if ( load( theReferenceId ) == null )
			return( null );
		
		if ( childId != null )
		{
			QuestionBusinessDelegate childDelegate 	= QuestionBusinessDelegate.getQuestionInstance();
			TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();			
			Question child 							= null;

			try
			{
				child = childDelegate.getQuestion( new QuestionPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "TheReferenceController:saveLastQuestionAnswered() failed to get Question using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			theReference.setLastQuestionAnswered( child );
		
			try
			{
				// save it
				parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:saveLastQuestionAnswered() failed saving parent TheReference - " + exc.getMessage() );
			}
		}
		
		return theReference;
	}

    /**
     * delete LastQuestionAnswered on TheReference
     * @param		Long theReferenceId
     * @return		TheReference
     */     
	@RequestMapping("/deleteLastQuestionAnswered")
	public TheReference deleteLastQuestionAnswered( @RequestParam(value="theReference.theReferenceId", required=true) Long theReferenceId )
	
	{
		if ( load( theReferenceId ) == null )
			return( null );

		if ( theReference.getLastQuestionAnswered() != null )
		{
			QuestionPrimaryKey pk = theReference.getLastQuestionAnswered().getQuestionPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			theReference.setLastQuestionAnswered( null );
			try
			{
				TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();

				// save it
				theReference = parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:deleteLastQuestionAnswered() failed to save TheReference - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				QuestionBusinessDelegate childDelegate = QuestionBusinessDelegate.getQuestionInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:deleteLastQuestionAnswered() failed  - " + exc.getMessage() );
			}
		}
		
		return theReference;
	}
	

    /**
     * save Answers on TheReference
     * @param		Long theReferenceId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		TheReference
     */     
	@RequestMapping("/saveAnswers")
	public TheReference saveAnswers( @RequestParam(value="theReference.theReferenceId", required=false) Long theReferenceId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( theReferenceId ) == null )
			return( null );
		
		AnswerPrimaryKey pk 					= null;
		Answer child							= null;
		AnswerBusinessDelegate childDelegate 	= AnswerBusinessDelegate.getAnswerInstance();
		TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new AnswerPrimaryKey( childId );
			
			try
			{
				// find the Answer
				child = childDelegate.getAnswer( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "TheReferenceController:saveAnswers() failed get child Answer using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the Answers 
			theReference.getAnswers().add( child );				
		}
		else
		{
			// clear out the Answers but 
			theReference.getAnswers().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new AnswerPrimaryKey( id );
					try
					{
						// find the Answer
						child = childDelegate.getAnswer( pk );
						// add it to the Answers List
						theReference.getAnswers().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "TheReferenceController:saveAnswers() failed get child Answer using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the TheReference
			parentDelegate.saveTheReference( theReference );
		}
		catch( Exception exc )
		{
			LOGGER.info( "TheReferenceController:saveAnswers() failed saving parent TheReference - " + exc.getMessage() );
		}

		return theReference;
	}

    /**
     * delete Answers on TheReference
     * @param		Long theReferenceId
     * @param		Long[] childIds
     * @return		TheReference
     */     	
	@RequestMapping("/deleteAnswers")
	public TheReference deleteAnswers( @RequestParam(value="theReference.theReferenceId", required=true) Long theReferenceId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( theReferenceId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			AnswerPrimaryKey pk 					= null;
			AnswerBusinessDelegate childDelegate 	= AnswerBusinessDelegate.getAnswerInstance();
			TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();
			Set<Answer> children					= theReference.getAnswers();
			Answer child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new AnswerPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getAnswer( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "TheReferenceController:deleteAnswers() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of Answer back to the theReference
			theReference.setAnswers( children );			
			// save it 
			try
			{
				theReference = parentDelegate.saveTheReference( theReference );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "TheReferenceController:deleteAnswers() failed to save the TheReference - " + exc.getMessage() );
			}
		}
		
		return theReference;
	}


    /**
     * Handles creating a TheReference BO
     * @return		TheReference
     */
    protected TheReference create()
    {
        try
        {       
			this.theReference = TheReferenceBusinessDelegate.getTheReferenceInstance().createTheReference( theReference );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "TheReferenceController:create() - exception TheReference - " + exc.getMessage());        	
        	return null;
        }
        
        return this.theReference;
    }

    /**
     * Handles updating a TheReference BO
     * @return		TheReference
     */    
    protected TheReference update()
    {
    	// store provided data
        TheReference tmp = theReference;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	theReference.copyShallow( tmp );
    	
        try
        {                        	        
			// create the TheReferenceBusiness Delegate            
			TheReferenceBusinessDelegate delegate = TheReferenceBusinessDelegate.getTheReferenceInstance();
            this.theReference = delegate.saveTheReference( theReference );
            
            if ( this.theReference != null )
                LOGGER.info( "TheReferenceController:update() - successfully updated TheReference - " + theReference.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "TheReferenceController:update() - successfully update TheReference - " + exc.getMessage());        	
        	return null;
        }
        
        return this.theReference;
        
    }


    /**
     * Returns true if the theReference is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( theReference != null && theReference.getTheReferencePrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected TheReference load()
    {
    	return( load( new Long( theReference.getTheReferencePrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected TheReference theReference = null;
    private static final Logger LOGGER = Logger.getLogger(TheReference.class.getName());
    
}


