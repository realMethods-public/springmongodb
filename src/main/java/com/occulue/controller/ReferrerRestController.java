/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity Referrer.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/Referrer")
public class ReferrerRestController extends BaseSpringRestController
{

    /**
     * Handles saving a Referrer BO.  if not key provided, calls create, otherwise calls save
     * @param		Referrer referrer
     * @return		Referrer
     */
	@RequestMapping("/save")
    public Referrer save( @RequestBody Referrer referrer )
    {
    	// assign locally
    	this.referrer = referrer;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a Referrer BO
     * @param		Long referrerId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="referrer.referrerId", required=false) Long referrerId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	ReferrerBusinessDelegate delegate = ReferrerBusinessDelegate.getReferrerInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new ReferrerPrimaryKey( referrerId ) );
        		LOGGER.info( "ReferrerController:delete() - successfully deleted Referrer with key " + referrer.getReferrerPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ReferrerPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "ReferrerController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferrerController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a Referrer BO
     * @param		Long referrerId
     * @return		Referrer
     */    
    @RequestMapping("/load")
    public Referrer load( @RequestParam(value="referrer.referrerId", required=true) Long referrerId )
    {    	
        ReferrerPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****Referrer.load pk is " + referrerId );
        	if ( referrerId != null )
        	{
        		pk = new ReferrerPrimaryKey( referrerId );
        		
        		// load the Referrer
	            this.referrer = ReferrerBusinessDelegate.getReferrerInstance().getReferrer( pk );
	            
	            LOGGER.info( "ReferrerController:load() - successfully loaded - " + this.referrer.toString() );             
			}
			else
			{
	            LOGGER.info( "ReferrerController:load() - unable to locate the primary key as an attribute or a selection for - " + referrer.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "ReferrerController:load() - failed to load Referrer using Id " + referrerId + ", " + exc.getMessage() );
            return null;
        }

        return referrer;

    }

    /**
     * Handles loading all Referrer business objects
     * @return		List<Referrer>
     */
    @RequestMapping("/loadAll")
    public List<Referrer> loadAll()
    {                
        List<Referrer> referrerList = null;
        
    	try
        {                        
            // load the Referrer
            referrerList = ReferrerBusinessDelegate.getReferrerInstance().getAllReferrer();
            
            if ( referrerList != null )
                LOGGER.info(  "ReferrerController:loadAllReferrer() - successfully loaded all Referrers" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "ReferrerController:loadAll() - failed to load all Referrers - " + exc.getMessage() );
        	return null;
            
        }

        return referrerList;
                            
    }


// findAllBy methods



    /**
     * save Comments on Referrer
     * @param		Long referrerId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		Referrer
     */     
	@RequestMapping("/saveComments")
	public Referrer saveComments( @RequestParam(value="referrer.referrerId", required=false) Long referrerId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( referrerId ) == null )
			return( null );
		
		CommentPrimaryKey pk 					= null;
		Comment child							= null;
		CommentBusinessDelegate childDelegate 	= CommentBusinessDelegate.getCommentInstance();
		ReferrerBusinessDelegate parentDelegate = ReferrerBusinessDelegate.getReferrerInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new CommentPrimaryKey( childId );
			
			try
			{
				// find the Comment
				child = childDelegate.getComment( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ReferrerController:saveComments() failed get child Comment using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the Comments 
			referrer.getComments().add( child );				
		}
		else
		{
			// clear out the Comments but 
			referrer.getComments().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new CommentPrimaryKey( id );
					try
					{
						// find the Comment
						child = childDelegate.getComment( pk );
						// add it to the Comments List
						referrer.getComments().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "ReferrerController:saveComments() failed get child Comment using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the Referrer
			parentDelegate.saveReferrer( referrer );
		}
		catch( Exception exc )
		{
			LOGGER.info( "ReferrerController:saveComments() failed saving parent Referrer - " + exc.getMessage() );
		}

		return referrer;
	}

    /**
     * delete Comments on Referrer
     * @param		Long referrerId
     * @param		Long[] childIds
     * @return		Referrer
     */     	
	@RequestMapping("/deleteComments")
	public Referrer deleteComments( @RequestParam(value="referrer.referrerId", required=true) Long referrerId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( referrerId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			CommentPrimaryKey pk 					= null;
			CommentBusinessDelegate childDelegate 	= CommentBusinessDelegate.getCommentInstance();
			ReferrerBusinessDelegate parentDelegate = ReferrerBusinessDelegate.getReferrerInstance();
			Set<Comment> children					= referrer.getComments();
			Comment child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new CommentPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getComment( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "ReferrerController:deleteComments() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of Comment back to the referrer
			referrer.setComments( children );			
			// save it 
			try
			{
				referrer = parentDelegate.saveReferrer( referrer );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "ReferrerController:deleteComments() failed to save the Referrer - " + exc.getMessage() );
			}
		}
		
		return referrer;
	}


    /**
     * Handles creating a Referrer BO
     * @return		Referrer
     */
    protected Referrer create()
    {
        try
        {       
			this.referrer = ReferrerBusinessDelegate.getReferrerInstance().createReferrer( referrer );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferrerController:create() - exception Referrer - " + exc.getMessage());        	
        	return null;
        }
        
        return this.referrer;
    }

    /**
     * Handles updating a Referrer BO
     * @return		Referrer
     */    
    protected Referrer update()
    {
    	// store provided data
        Referrer tmp = referrer;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	referrer.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ReferrerBusiness Delegate            
			ReferrerBusinessDelegate delegate = ReferrerBusinessDelegate.getReferrerInstance();
            this.referrer = delegate.saveReferrer( referrer );
            
            if ( this.referrer != null )
                LOGGER.info( "ReferrerController:update() - successfully updated Referrer - " + referrer.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ReferrerController:update() - successfully update Referrer - " + exc.getMessage());        	
        	return null;
        }
        
        return this.referrer;
        
    }


    /**
     * Returns true if the referrer is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( referrer != null && referrer.getReferrerPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected Referrer load()
    {
    	return( load( new Long( referrer.getReferrerPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected Referrer referrer = null;
    private static final Logger LOGGER = Logger.getLogger(Referrer.class.getName());
    
}


