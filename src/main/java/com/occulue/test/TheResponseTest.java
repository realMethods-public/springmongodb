/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/**
 * Test TheResponse class.
 *
 * @author    dev@realmethods.com
 */
public class TheResponseTest
{

// constructors

    public TheResponseTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a TheResponse, through a TheResponseTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on TheResponseTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on TheResponseTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new TheResponse.
     *
     * @return    TheResponse
     */
    public TheResponse testCreate()
    throws Throwable
    {
        TheResponse businessObject = null;

    	{
	        LOGGER.info( "TheResponseTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a TheResponse");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a TheResponse" );
	
	        try 
	        {            
	            businessObject = TheResponseBusinessDelegate.getTheResponseInstance().createTheResponse( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (TheResponsePrimaryKey)businessObject.getTheResponsePrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a TheResponse with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a TheResponse.
     *
     * @return    TheResponse  
     */
    public TheResponse testRead()
    throws Throwable
    {
        LOGGER.info( "TheResponseTest:testRead()" );
        LOGGER.info( "-- Reading a previously created TheResponse" );

        TheResponse businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read TheResponse with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = TheResponseBusinessDelegate.getTheResponseInstance().getTheResponse( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found TheResponse " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a TheResponse.
     *
     * @return    TheResponse
     */
    public TheResponse testUpdate()
    throws Throwable
    {

        LOGGER.info( "TheResponseTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a TheResponse." );

        StringBuilder msg = new StringBuilder( "Failed to update a TheResponse : " );        
        TheResponse businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created TheResponse." );
            
            // for use later on...
            thePrimaryKey = (TheResponsePrimaryKey)businessObject.getTheResponsePrimaryKey();
            
            TheResponseBusinessDelegate proxy = TheResponseBusinessDelegate.getTheResponseInstance();            
            businessObject = proxy.saveTheResponse( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved TheResponse - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a TheResponse.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "TheResponseTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created TheResponse." );
        
        try
        {
            TheResponseBusinessDelegate.getTheResponseInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted TheResponse with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete TheResponse with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all TheResponses.
     *
     * @return    Collection
     */
    public ArrayList<TheResponse> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "TheResponseTest:testGetAll() - Retrieving Collection of TheResponses:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all TheResponse : " );        
        ArrayList<TheResponse> collection  = null;

        try
        {
            // call the static get method on the TheResponseBusinessDelegate
            collection = TheResponseBusinessDelegate.getTheResponseInstance().getAllTheResponse();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            TheResponse currentBO  = null;            
	            Iterator<TheResponse> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getTheResponsePrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public TheResponseTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate TheResponse
     * 
     * @return    TheResponse
     */    
    protected TheResponse getNewBO()
    {
        TheResponse newBO = new TheResponse();

// AIB : \#defaultBOOutput() 
newBO.setResponseText(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
newBO.setGotoQuestionId(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected TheResponsePrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(TheResponse.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}
