/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/**
 * Test Question class.
 *
 * @author    dev@realmethods.com
 */
public class QuestionTest
{

// constructors

    public QuestionTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a Question, through a QuestionTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on QuestionTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on QuestionTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new Question.
     *
     * @return    Question
     */
    public Question testCreate()
    throws Throwable
    {
        Question businessObject = null;

    	{
	        LOGGER.info( "QuestionTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a Question");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a Question" );
	
	        try 
	        {            
	            businessObject = QuestionBusinessDelegate.getQuestionInstance().createQuestion( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (QuestionPrimaryKey)businessObject.getQuestionPrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a Question with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a Question.
     *
     * @return    Question  
     */
    public Question testRead()
    throws Throwable
    {
        LOGGER.info( "QuestionTest:testRead()" );
        LOGGER.info( "-- Reading a previously created Question" );

        Question businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read Question with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = QuestionBusinessDelegate.getQuestionInstance().getQuestion( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found Question " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a Question.
     *
     * @return    Question
     */
    public Question testUpdate()
    throws Throwable
    {

        LOGGER.info( "QuestionTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a Question." );

        StringBuilder msg = new StringBuilder( "Failed to update a Question : " );        
        Question businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created Question." );
            
            // for use later on...
            thePrimaryKey = (QuestionPrimaryKey)businessObject.getQuestionPrimaryKey();
            
            QuestionBusinessDelegate proxy = QuestionBusinessDelegate.getQuestionInstance();            
            businessObject = proxy.saveQuestion( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved Question - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a Question.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "QuestionTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created Question." );
        
        try
        {
            QuestionBusinessDelegate.getQuestionInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted Question with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete Question with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all Questions.
     *
     * @return    Collection
     */
    public ArrayList<Question> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "QuestionTest:testGetAll() - Retrieving Collection of Questions:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all Question : " );        
        ArrayList<Question> collection  = null;

        try
        {
            // call the static get method on the QuestionBusinessDelegate
            collection = QuestionBusinessDelegate.getQuestionInstance().getAllQuestion();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            Question currentBO  = null;            
	            Iterator<Question> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getQuestionPrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public QuestionTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate Question
     * 
     * @return    Question
     */    
    protected Question getNewBO()
    {
        Question newBO = new Question();

// AIB : \#defaultBOOutput() 
newBO.setWeight(
    new java.lang.Double( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
newBO.setQuestionText(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
newBO.setIdentifier(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
newBO.setMustBeAnswered(
    new Boolean( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
newBO.setResponseExclusive(
    new Boolean( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected QuestionPrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(Question.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}
